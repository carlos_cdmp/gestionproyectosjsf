/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gp.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Portatil
 */
@Entity
@Table(name = "INVITACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Invitacion.findAll", query = "SELECT i FROM Invitacion i"),
    @NamedQuery(name = "Invitacion.findByFecha", query = "SELECT i FROM Invitacion i WHERE i.fecha = :fecha"),
    @NamedQuery(name = "Invitacion.findByMensaje", query = "SELECT i FROM Invitacion i WHERE i.mensaje = :mensaje"),
    @NamedQuery(name = "Invitacion.findById", query = "SELECT i FROM Invitacion i WHERE i.id = :id")})
public class Invitacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Size(max = 150)
    @Column(name = "MENSAJE")
    private String mensaje;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private BigDecimal id;
    @JoinColumn(name = "IDPROYECTO", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Proyecto idproyecto;
    @JoinColumn(name = "IDUSER", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Usuario iduser;

    public Invitacion() {
    }

    public Invitacion(BigDecimal id) {
        this.id = id;
    }

    public Invitacion(BigDecimal id, Date fecha) {
        this.id = id;
        this.fecha = fecha;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public Proyecto getIdproyecto() {
        return idproyecto;
    }

    public void setIdproyecto(Proyecto idproyecto) {
        this.idproyecto = idproyecto;
    }

    public Usuario getIduser() {
        return iduser;
    }

    public void setIduser(Usuario iduser) {
        this.iduser = iduser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Invitacion)) {
            return false;
        }
        Invitacion other = (Invitacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gp.entity.Invitacion[ id=" + id + " ]";
    }
    
}
