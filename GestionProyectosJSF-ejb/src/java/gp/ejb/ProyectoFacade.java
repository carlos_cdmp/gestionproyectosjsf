/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gp.ejb;

import gp.entity.Proyecto;
import gp.entity.Usuario;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Portatil
 */
@Stateless
public class ProyectoFacade extends AbstractFacade<Proyecto> {

    @PersistenceContext(unitName = "GestionProyectosJSF-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProyectoFacade() {
        super(Proyecto.class);
    }
    public List<Proyecto> conLider(Usuario l) {
        List<Proyecto> misProyectos = new ArrayList<Proyecto>();
        misProyectos.clear();

        Query q;
        q = em.createQuery("SELECT p from Proyecto p WHERE p.uslider.id = :lid ");
        q.setParameter("lid", l.getId());
        return q.getResultList();
    }
    
    public List<Proyecto> proyectosDelUsuario(Usuario user) {
        Query q;
        q = em.createQuery(" SELECT p FROM Proyecto p WHERE :us MEMBER OF p.usuarioCollection").setParameter("us", user);
        List<Proyecto> lista = q.getResultList();
        return lista;
    }
    
    public Proyecto findById(BigDecimal id) {
        Query q;

        q = em.createNamedQuery("Proyecto.findById").setParameter("id", id);

        return (Proyecto) q.getSingleResult();
    }
    
}
