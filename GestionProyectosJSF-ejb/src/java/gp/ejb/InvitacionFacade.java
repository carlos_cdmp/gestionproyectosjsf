/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gp.ejb;

import gp.entity.Invitacion;
import gp.entity.Proyecto;
import gp.entity.Usuario;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Portatil
 */
@Stateless
public class InvitacionFacade extends AbstractFacade<Invitacion> {

    @PersistenceContext(unitName = "GestionProyectosJSF-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public InvitacionFacade() {
        super(Invitacion.class);
    }
    
    public Integer getMaxID() {
        Query q;
        BigDecimal b;
        Integer i = 0;
        q = em.createQuery("Select max(i.id) from Invitacion i");

        try{
           b = (BigDecimal)q.getSingleResult();
           i = b.intValue();
        } catch (Exception e) {
            
        }   
        return i;
    }

    public List<Invitacion> getInvitacionesUsuario(Usuario u) {
        List<Invitacion> invitaciones;
        Query q;
        
        q = em.createQuery("SELECT i FROM Invitacion i WHERE i.iduser = :id").setParameter("id", u);
        
        invitaciones = q.getResultList();
        
        return invitaciones;
    }
    
    public Invitacion getInvitacionById(Integer id){
        Query q;

        q = em.createNamedQuery("Invitacion.findById").setParameter("id", id);

        return (Invitacion) q.getSingleResult();
        
    }
    
    public Invitacion getInvitacionByUserPoyect(Usuario u, Proyecto p){
        Query q;
        
        q = em.createQuery("Select i from Invitacion i where i.iduser = :user and i.idproyecto = :proy")
                .setParameter("user", u).setParameter("proy", p); 
        
        return (Invitacion) q.getSingleResult();
    }
    
}
