/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gp.ejb;

import gp.entity.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Portatil
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {

    @PersistenceContext(unitName = "GestionProyectosJSF-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }

    public Usuario usuarioConUsername(String user) {
        try {
            Query q;
            q = em.createQuery("Select u from Usuario u WHERE u.username LIKE :user ").setParameter("user", user);

            return (Usuario) q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public String recPassword(String usuario, String email) {
        Query q;
        q = em.createQuery("Select u.password from Usuario u WHERE u.email LIKE :email AND u.username LIKE :user ");
        q.setParameter("email", email);
        q.setParameter("user", usuario);

        return (String) q.getSingleResult();
    }

    public List<Usuario> ConContraseña(String user, String pass) {
        Query q;
        q = em.createQuery("Select u from Usuario u WHERE u.password LIKE :pass AND u.username LIKE :user ").setParameter("user", user).setParameter("pass", pass);

        return q.getResultList();

    }
}
