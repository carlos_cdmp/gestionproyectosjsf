/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gp.bean;

import gp.ejb.MensajeFacade;
import gp.ejb.ProyectoFacade;
import gp.entity.Mensaje;
import gp.entity.Proyecto;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;



/**
 *
 * @author Francisco
 */
@ManagedBean
@RequestScoped
public class MensajeBean {

    @EJB
    private MensajeFacade mensajeFacade;
    @EJB
    private ProyectoFacade proyectoFacade;
    
    @ManagedProperty(value = "#{usuarioBean}")
    protected UsuarioBean usuarioBean;
    
    protected List<Mensaje> listaMensaje;
    protected Mensaje mensajeSeleccionado = new Mensaje();
    /**
     * Creates a new instance of MensajeBean
     */
    public MensajeBean() {
    }
    
    @PostConstruct
    public void init(){
        
        listaMensaje = (List<Mensaje>) usuarioBean.proyectoSeleccionado.getMensajeCollection();
       
    }

    public void crearMensaje(){
        Date fecha = new Date();
        mensajeSeleccionado.setFecha(fecha);
        mensajeSeleccionado.setIdproyecto(usuarioBean.proyectoSeleccionado);
        mensajeSeleccionado.setIduser(usuarioBean.usuarioSeleccionado);
        mensajeFacade.create(mensajeSeleccionado);
        this.listaMensaje.add(mensajeSeleccionado);
        usuarioBean.proyectoSeleccionado.setMensajeCollection(listaMensaje);
        this.proyectoFacade.edit(usuarioBean.proyectoSeleccionado);
        mensajeSeleccionado = new Mensaje();
    }
    
    public String doAtras(){
        this.usuarioBean.proyectoSeleccionado = new Proyecto();
        return "misProyectos";
    }

    public List<Mensaje> getListaMensaje() {
        return listaMensaje;
    }

    public void setListaMensaje(List<Mensaje> listaMensaje) {
        this.listaMensaje = listaMensaje;
    }

    public Mensaje getMensajeSeleccionado() {
        return mensajeSeleccionado;
    }

    public void setMensajeSeleccionado(Mensaje mensajeSeleccionado) {
        this.mensajeSeleccionado = mensajeSeleccionado;
    }

    public UsuarioBean getUsuarioBean() {
        return usuarioBean;
    }

    public void setUsuarioBean(UsuarioBean usuarioBean) {
        this.usuarioBean = usuarioBean;
    }
    
    
}
