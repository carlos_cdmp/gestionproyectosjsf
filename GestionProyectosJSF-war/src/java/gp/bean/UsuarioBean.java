package gp.bean;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import gp.ejb.UsuarioFacade;
import gp.entity.Proyecto;
import gp.entity.Tarea;
import gp.entity.Usuario;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.event.AjaxBehaviorEvent;

/**
 *
 * @author Portatil
 */
@SessionScoped
@ManagedBean
public class UsuarioBean implements Serializable {

    @EJB
    private UsuarioFacade usuarioFacade;

    protected List<Usuario> listaUsuarios;
    protected Usuario usuarioSeleccionado;
    protected String inputContraseña;
    protected String verificarContraseña;
    protected String inputUsuario;
    protected String errorUsuario;

    protected String mensajeVerificacion;
    protected boolean coinciden;

    protected Proyecto proyectoSeleccionado;
    protected Tarea tareaSeleccionada;
    protected String nombreUsuarioTarea;

    public String getNombreUsuarioTarea() {
        return nombreUsuarioTarea;
    }

    public void setNombreUsuarioTarea(String nombreUsuarioTarea) {
        this.nombreUsuarioTarea = nombreUsuarioTarea;
    }

    public Tarea getTareaSeleccionada() {
        return tareaSeleccionada;
    }

    public void setTareaSeleccionada(Tarea tareaSeleccionada) {
        this.tareaSeleccionada = tareaSeleccionada;
    }

    public Proyecto getProyectoSeleccionado() {
        return proyectoSeleccionado;
    }

    public void setProyectoSeleccionado(Proyecto proyectoSeleccionado) {
        this.proyectoSeleccionado = proyectoSeleccionado;
    }

    /**
     * Creates a new instance of UsuarioBean
     */
    public UsuarioBean() {

    }

    public String doVerificarPass() {
        String s = "";
        if (coinciden) {
            s = "Correcto";
            coinciden = true;
        } else {
            s = "Las contraseñas no coinciden";
            coinciden = false;
        }
        return s;
    }

    @PostConstruct
    public void init() {
        setListaUsuarios(usuarioFacade.findAll());
        List<Usuario> lu = (getListaUsuarios());
        setErrorUsuario("");
        coinciden = false;
        mensajeVerificacion = "Necesario introducir contraseña";

    }

    public void doLimpiarSeleccionado() {
        setUsuarioSeleccionado(new Usuario(BigDecimal.ONE));
        inputContraseña="";
        verificarContraseña="";
    }

    public String doBorrar(Usuario u) {
        usuarioFacade.remove(u);
        listaUsuarios.remove(u);
        return "listaUsuarios";
    }
    
    public String doSalir(){
        this.usuarioSeleccionado = null;
        return "login";
    }

    public String doEditar(Usuario u) {
        usuarioFacade.edit(u);
        init();
        return "paginaPrincipal";
    }

    public String doGuardar() {
        usuarioSeleccionado.setPassword(inputContraseña);
        usuarioFacade.create(usuarioSeleccionado);
        init();
        doLimpiarSeleccionado();
        return "login";
    }

    public String doIrChat(Proyecto p){
        this.proyectoSeleccionado = p;
        return "chat";
    }
    
    public String doLogin() {
        usuarioSeleccionado = null;
        String res = "login";
        List<Usuario> posibleLogin = usuarioFacade.ConContraseña(inputUsuario, inputContraseña);
        if (posibleLogin != null && posibleLogin.size() == 1) {
            setUsuarioSeleccionado(posibleLogin.get(0));
            Usuario u = getUsuarioSeleccionado();
            res = "paginaPrincipal";
            setErrorUsuario("");
        } else {
            setErrorUsuario("Usuario/Contraseña incorrectos");
        }
        return res;
    }

    public void validarPass(AjaxBehaviorEvent event) {
        
        coinciden =  inputContraseña.equals(verificarContraseña);
        
        if (coinciden){
            mensajeVerificacion = "Contraseña correcta";
        }else{
            mensajeVerificacion = "Las contraseñas no coinciden";
        }
        if (inputContraseña.equals("")){
            mensajeVerificacion = "Es necesario introducir una contraseña en el campo CONTRASEÑA";
            coinciden = false;
        }
        
}
    
    
    public Usuario getUsuarioSeleccionado() {
        return usuarioSeleccionado;
    }

    public void setUsuarioSeleccionado(Usuario usuarioSeleccionado) {
        this.usuarioSeleccionado = usuarioSeleccionado;
    }

    public List<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(List<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public String getInputContraseña() {
        return inputContraseña;
    }

    public void setInputContraseña(String inputContraseña) {
        this.inputContraseña = inputContraseña;
    }

    public String getInputUsuario() {
        return inputUsuario;
    }

    public void setInputUsuario(String inputUsuario) {
        this.inputUsuario = inputUsuario;
    }

    public String getErrorUsuario() {
        return errorUsuario;
    }

    public void setErrorUsuario(String errorUsuario) {
        this.errorUsuario = errorUsuario;
    }

    public String getVerificarContraseña() {
        return verificarContraseña;
    }

    public void setVerificarContraseña(String verificarContraseña) {
        this.verificarContraseña = verificarContraseña;
    }

    public boolean isCoinciden() {
        return coinciden;
    }

    public void setCoinciden(boolean coinciden) {
        this.coinciden = coinciden;
    }

    public String getMensajeVerificacion() {
        return mensajeVerificacion;
    }

    public void setMensajeVerificacion(String mensajeVerificacion) {
        this.mensajeVerificacion = mensajeVerificacion;
    }

    
}
