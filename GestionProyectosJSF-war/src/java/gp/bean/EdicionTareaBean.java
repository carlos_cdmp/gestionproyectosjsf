/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gp.bean;

import gp.ejb.ProyectoFacade;
import gp.ejb.TareaFacade;
import gp.ejb.UsuarioFacade;
import gp.entity.Tarea;
import gp.entity.Usuario;
import java.math.BigDecimal;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author pablo
 */
@ManagedBean
@RequestScoped
public class EdicionTareaBean {

    @EJB
    private ProyectoFacade proyectoFacade;

    @EJB
    private UsuarioFacade usuarioFacade;

    @ManagedProperty(value = "#{usuarioBean}")
    protected UsuarioBean usuarioBean;
    @EJB
    private TareaFacade tareaFacade;

    public UsuarioBean getUsuarioBean() {
        return usuarioBean;
    }

    public void setUsuarioBean(UsuarioBean usuarioBean) {
        this.usuarioBean = usuarioBean;
    }

    /**
     * Creates a new instance of EdicionTareaBean
     */
    public EdicionTareaBean() {
    }

    public String doGuardar() {
        if (this.usuarioBean.tareaSeleccionada.getId() != null) {
            this.usuarioBean.tareaSeleccionada.setId2(this.usuarioFacade.usuarioConUsername(this.usuarioBean.nombreUsuarioTarea));
            this.tareaFacade.edit(usuarioBean.tareaSeleccionada);
        } else {
            this.usuarioBean.tareaSeleccionada.setId(BigDecimal.ONE);
            this.usuarioBean.tareaSeleccionada.setId2(this.usuarioFacade.usuarioConUsername(this.usuarioBean.nombreUsuarioTarea));
            this.usuarioBean.tareaSeleccionada.setId1(this.usuarioBean.proyectoSeleccionado);
            this.tareaFacade.create(usuarioBean.tareaSeleccionada);
            this.usuarioBean.proyectoSeleccionado.getTareaCollection().add(usuarioBean.tareaSeleccionada);
            this.proyectoFacade.edit(usuarioBean.proyectoSeleccionado);
        }
        return "listaTareas";
    }

}
