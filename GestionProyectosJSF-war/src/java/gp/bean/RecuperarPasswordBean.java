/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gp.bean;

import gp.ejb.UsuarioFacade;
import gp.entity.Usuario;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author pablo
 */
@ManagedBean
@RequestScoped
public class RecuperarPasswordBean {

    @EJB
    private UsuarioFacade usuarioFacade;

    /**
     * Creates a new instance of RecuperarPasswordBean
     */
    protected String usuario;
    protected String email;
    protected String password;
    protected String error;
    protected Usuario usuarioSeleccionado;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Usuario getUsuarioSeleccionado() {
        return usuarioSeleccionado;
    }

    public void setUsuarioSeleccionado(Usuario usuarioSeleccionado) {
        this.usuarioSeleccionado = usuarioSeleccionado;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RecuperarPasswordBean() {
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String devolverPassword() {
        String salida;
        this.error = "";
        this.usuarioSeleccionado = this.usuarioFacade.usuarioConUsername(this.usuario);
        if (!this.usuarioSeleccionado.getEmail().equals(this.email)) {
            error = "Email incorrecto. Vuelva a introducir los datos";
            salida = "recuperarPassword";
        } else {
            this.password = this.usuarioFacade.recPassword(this.usuario, this.email);
            salida = "mostrarPassRec";
        }

        return salida;
    }
}
