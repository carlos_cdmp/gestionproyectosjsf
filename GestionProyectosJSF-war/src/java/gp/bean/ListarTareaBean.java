/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gp.bean;

import gp.ejb.ProyectoFacade;
import gp.ejb.TareaFacade;
import gp.entity.Proyecto;
import gp.entity.Tarea;
import java.math.BigDecimal;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author pablo
 */
@ManagedBean
@RequestScoped
public class ListarTareaBean {

    @EJB
    private ProyectoFacade proyectoFacade;

    @ManagedProperty(value = "#{usuarioBean}")
    protected UsuarioBean usuarioBean;

    public UsuarioBean getUsuarioBean() {
        return usuarioBean;
    }

    public void setUsuarioBean(UsuarioBean usuarioBean) {
        this.usuarioBean = usuarioBean;
    }

    @EJB
    private TareaFacade tareaFacade;

    protected List<Tarea> listaTareas;
    protected Proyecto proyecto;
    //protected Tarea tareaSeleccionada;

    public List<Tarea> getListaTareas() {
        return listaTareas;
    }

    public void setListaTareas(List<Tarea> listaTareas) {
        this.listaTareas = listaTareas;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * Creates a new instance of TareaBean
     */
    public ListarTareaBean() {
    }

    @PostConstruct
    public void init() {
        this.proyecto = this.proyectoFacade.findById(usuarioBean.proyectoSeleccionado.getId());
        this.listaTareas = (List<Tarea>) this.proyecto.getTareaCollection();
    }

    public void resetTarea() {
        usuarioBean.tareaSeleccionada = new Tarea();
    }

    public String nuevaTarea() {
        this.resetTarea();
        return "edicionTarea";
    }

    public String editarTarea(Tarea tarea) {
        usuarioBean.tareaSeleccionada = tarea;
        return "edicionTarea";
    }

    public String doBorrar(Tarea tarea) {
        this.tareaFacade.remove(tarea);
        this.listaTareas.remove(tarea);
        this.usuarioBean.proyectoSeleccionado.getTareaCollection().remove(tarea);
        this.proyectoFacade.edit(usuarioBean.proyectoSeleccionado);
        return "listaTareas";
    }
    
    public String doVolverAdministrarProyectos(){
        return "administrarProyectos";
    }
}
