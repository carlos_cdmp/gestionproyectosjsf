/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gp.bean;

import gp.ejb.ProyectoFacade;
import gp.entity.Proyecto;
import gp.entity.Tarea;
import gp.entity.Usuario;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

/**
 *
 * @author Portatil
 */
@ManagedBean
@RequestScoped
public class ProyectoBean {
 @EJB
    private ProyectoFacade proyectoFacade;
    
    protected List<Proyecto> listaProyectos;
    protected Proyecto proyectoSeleccionado;
    protected java.util.Date fecha;
    
    @ManagedProperty(value = "#{usuarioBean}")
    protected UsuarioBean usuarioBean;
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    /**
     * Creates a new instance of ProyectoBean
     */
    public ProyectoBean() {
    }
    
    @PostConstruct
    public void init() {
        setListaProyectos(proyectoFacade.findAll());
        proyectoSeleccionado = new Proyecto(BigDecimal.ONE);
    }
    
    //Para que desaparezcan los posibles datos introducidos
    public void limpiarSeleccionado(){
        proyectoSeleccionado = new Proyecto(BigDecimal.ONE);
    }
    public String doCrearProyecto(){
        Proyecto p = proyectoSeleccionado;
        Usuario lid = usuarioBean.getUsuarioSeleccionado();
        p.setUslider(lid);
        LinkedList listaUs = new LinkedList<Usuario>();
        listaUs.add(lid);
        lid.getProyectoCollection().add(p);
        p.setUsuarioCollection(listaUs);
        p.setTareaCollection(new LinkedList<Tarea>());
        proyectoFacade.create(p);
        listaProyectos.add(p);
        usuarioBean.setProyectoSeleccionado(p);
        return "invitarUsuario";
    }
    
    public String doBorrar(Proyecto p){
        proyectoFacade.remove(p);
        listaProyectos.remove(p);
        return "administrarProyectos";
    }
    
    public String obtenerProyectoParaTareas(Proyecto p){
        this.usuarioBean.proyectoSeleccionado = p;
        return "listaTareas";
    }
    
    public void doGetProyectosParticipa(){
        Usuario logged = usuarioBean.getUsuarioSeleccionado();
        setListaProyectos(proyectoFacade.proyectosDelUsuario(logged));
    }
    
    public void doGetProyectosLidera(){
        Usuario logged = usuarioBean.getUsuarioSeleccionado();
        setListaProyectos(proyectoFacade.conLider(logged));
    }

    public List<Proyecto> getListaProyectos() {
        return listaProyectos;
    }

    public void setListaProyectos(List<Proyecto> listaProyectos) {
        this.listaProyectos = listaProyectos;
    }

    public Proyecto getProyectoSeleccionado() {
        return proyectoSeleccionado;
    }

    public void setProyectoSeleccionado(Proyecto proyectoSeleccionado) {
        this.proyectoSeleccionado = proyectoSeleccionado;
    }

    public UsuarioBean getUsuarioBean() {
        return usuarioBean;
    }

    public void setUsuarioBean(UsuarioBean usuarioBean) {
        this.usuarioBean = usuarioBean;
    }
    
    public String doInvitar(Proyecto p){
        this.usuarioBean.setProyectoSeleccionado(p);
        return "invitarUsuario";
    }   
    
}
