/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gp.bean;

import gp.ejb.InvitacionFacade;
import gp.ejb.ProyectoFacade;
import gp.ejb.UsuarioFacade;
import gp.entity.Invitacion;
import gp.entity.Proyecto;
import gp.entity.Usuario;
import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.event.AjaxBehaviorEvent;

/**
 *
 * @author franciscojose
 */
@ManagedBean
@RequestScoped
public class InvitacionBean {

    @EJB
    InvitacionFacade invitacionFacade;
    @EJB
    UsuarioFacade usuarioFacade;
    @EJB
    ProyectoFacade proyectoFacade;
    
    @ManagedProperty(value = "#{usuarioBean}")
    protected UsuarioBean  usuarioBean;
            //Accedo a estas variables para coger el usauario y el proyecto
    
    protected Invitacion invitacion = new Invitacion();
    protected String nombreUsuario;
    protected String mensajeVerificacion;
    /**
     * Creates a new instance of InvitacionBean
     */
    public InvitacionBean() {
    }
    
    public String doInvitar(){
        invitacion.setId(new BigDecimal(invitacionFacade.getMaxID() + 1));
        invitacion.setIdproyecto(usuarioBean.getProyectoSeleccionado());
        invitacion.setIduser(usuarioFacade.usuarioConUsername(nombreUsuario));
        invitacionFacade.create(invitacion);
        invitacion = new Invitacion();
        nombreUsuario="";

        return "invitarUsuario";
    }
    
    public void doVerificarUsuario(AjaxBehaviorEvent event){
        if(nombreUsuario.equals("")){
            this.mensajeVerificacion = "Es necesario introducir nombre de usuario";
        }
        
        if(usuarioFacade.usuarioConUsername(nombreUsuario) == null){
            this.mensajeVerificacion = "Usuario no válido";
        } else {
            this.mensajeVerificacion = "";
        }
    }
    
    public String doFin(){
        //Establezco el proyecto seleccionado a uno vacio
        usuarioBean.proyectoSeleccionado = new Proyecto();
        return "paginaPrincipal";
    }

    public Invitacion getInvitacion() {
        return invitacion;
    }

    public void setInvitacion(Invitacion invitacion) {
        this.invitacion = invitacion;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public UsuarioBean getUsuarioBean() {
        return usuarioBean;
    }

    public void setUsuarioBean(UsuarioBean usuarioBean) {
        this.usuarioBean = usuarioBean;
    }

    public String getMensajeVerificacion() {
        return mensajeVerificacion;
    }

    public void setMensajeVerificacion(String mensajeVerificacion) {
        this.mensajeVerificacion = mensajeVerificacion;
    }
    
    
    
    public String doAceptar(Invitacion i){
        Collection<Usuario> usuariosProyecto;
        Collection<Proyecto> proyectosUsuario;
        
        //Obtengo el proyecto al que me voy a unir
        Proyecto p = i.getIdproyecto();
        
        //modifico la lista de usuarios del proyecto para incluir al usuario de la sesion
        usuariosProyecto = p.getUsuarioCollection();
        usuariosProyecto.add(usuarioBean.getUsuarioSeleccionado());
        p.setUsuarioCollection(usuariosProyecto);
        
        //Incluyo el proyecto en la coleccion de proyectos del usuario 
        proyectosUsuario = usuarioBean.getUsuarioSeleccionado().getProyectoCollection();
        proyectosUsuario.add(p);
        usuarioBean.getUsuarioSeleccionado().setProyectoCollection(proyectosUsuario);
        
        //Borro la invitacion de la lista de invitaciones pendientes del proyecto
        Collection<Invitacion> invitaciones = p.getInvitacionCollection();
        invitaciones.remove(i);
        p.setInvitacionCollection(invitaciones);
        
        //Elimino la invitacion de la lista de invitaciones del usuario
        invitaciones = usuarioBean.usuarioSeleccionado.getInvitacionCollection();
        invitaciones.remove(i);
        usuarioBean.usuarioSeleccionado.setInvitacionCollection(invitaciones);
        
        //Ejecuto los cambios en la BD
        invitacionFacade.remove(i);
        proyectoFacade.edit(p);
        usuarioFacade.edit(usuarioBean.getUsuarioSeleccionado());
        return "listadoInvitaciones";
    }
    
    public String doRechazar(Invitacion i){
        //Obtengo el proyecto correspondiente a la invitacion
        Proyecto p = i.getIdproyecto();
        
        //Borro la invitacion de la lista de invitaciones pendientes del proyecto
        Collection<Invitacion> invitaciones = p.getInvitacionCollection();
        invitaciones.remove(i);
        p.setInvitacionCollection(invitaciones);
        
        //Elimino la invitacion de la lista de invitaciones del usuario
        invitaciones = usuarioBean.usuarioSeleccionado.getInvitacionCollection();
        invitaciones.remove(i);
        usuarioBean.usuarioSeleccionado.setInvitacionCollection(invitaciones);
        
        //Ejecuto los cambios en la BD
        usuarioFacade.edit(usuarioBean.getUsuarioSeleccionado());
        invitacionFacade.remove(i);
        proyectoFacade.edit(p);
        return "listadoInvitaciones";
    }
}
